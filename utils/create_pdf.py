# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/7 下午2:56
# Description :
import json
import uuid

import requests
from django.conf import settings

from apis.models import PDFCover

HOST = settings.BACKEND
APIS = settings.APIS

def create_pdf(request):
    request_data = request.data
    url = HOST['DOWNLOAD']+APIS['DOWNLOAD']['PDF_POST']
    user_id = request.user_id
    data = {
        "name": request_data['familyName'],
        "content": "test",
        "type": get_family_format(request_data),
        "people": request_data['peopleName'],
        "user": user_id,
        "cover": get_cover(request_data['cover']).__str__(),
        "pdfData": json.dumps(request_data)
    }
    token_id = request.headers.get('Authorization')
    # token_id = ''
    # print(data)
    headers = {
        "Authorization":token_id
    }
    res = requests.post(url, data=data, headers=headers)
    print(res.status_code)
    print(res.json())

def get_family_format(request_data):
    """获取家族"""
    family = ''
    f = json.loads(request_data['format'])
    for key, value in f.items():
        if value != 0:
            family += (key+',')
    return family[:-1]

def get_cover(cover):
    """获取封面"""
    cover_obj = PDFCover.objects.get(id=cover)
    small_image = cover_obj.smallImage
    return small_image
