# Generated by Django 3.2.3 on 2021-06-25 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pdfimages',
            name='feature',
            field=models.CharField(default='', help_text='模板特点', max_length=255),
        ),
    ]
