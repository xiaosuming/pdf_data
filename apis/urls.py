# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/3 下午3:57
# Description :
from django.conf.urls.static import static
from django.urls import re_path, path
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from apis.views import TemplateTypes, PDFCovers, Template, PDFCover, PDFImagesView, PDFTemplateImages, CreatePDF, \
    CenterSeamsView, CenterSeamView, PDFTemplateImagesView

urlpatterns = [
    path('pdfData/templateTypes', csrf_exempt(TemplateTypes.as_view())),
    path('pdfData/pdfCovers', csrf_exempt(PDFCovers.as_view())),
    path('pdfData/templateType/<str:pk>', csrf_exempt(Template.as_view())),
    path('pdfData/pdfCovers/<int:pk>', csrf_exempt(PDFCover.as_view())),
    path('pdfData/pdfTemplateImages', csrf_exempt(PDFTemplateImages.as_view())),
    path('pdfData/pdfTemplateImages/<int:pk>', csrf_exempt(PDFTemplateImagesView.as_view())),
    path('pdfData/image', csrf_exempt(PDFImagesView.as_view())),
    path('pdfData', csrf_exempt(CreatePDF.as_view())),
    path('pdfData/centerSeams', csrf_exempt(CenterSeamsView.as_view())),
    path('pdfData/centerSeams/<int:pk>', csrf_exempt(CenterSeamView.as_view()))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


