from drf_yasg2 import openapi
from drf_yasg2.utils import swagger_auto_schema
from rest_framework import mixins, generics
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from apis.models import TemplateType, PDFImages, PDFCover as PC, CenterSeam
from apis.serializers import TemplateTypeSerializers, PDFCoverSerializers, PDFImagesSerializers, CenterSeamSerializers
from utils.create_pdf import create_pdf
from utils.return_mes import return_mes


class PDFTemplateImages(mixins.CreateModelMixin, generics.GenericAPIView):
    """
    get:
    获取所有的模板版式图片url以及对应关系
    post:
    添加模板版式图片和模板的对应关系
    """
    queryset = PDFImages.objects.all()
    serializer_class = PDFImagesSerializers

    def get(self, request):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(return_mes(code=0, data=serializer.data))

    parser_classes = (MultiPartParser,)
    def post(self, request):
        return self.create(request)


class PDFTemplateImagesView(mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                            generics.GenericAPIView):
    queryset = PDFImages.objects.all()
    serializer_class = PDFImagesSerializers
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def perform_destroy(self, instance):
        instance.bigImage.delete()
        instance.smallImage.delete()
        instance.delete()

    parser_classes = (MultiPartParser,)
    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class TemplateTypes(mixins.ListModelMixin, mixins.UpdateModelMixin,
                  mixins.CreateModelMixin, generics.GenericAPIView):
    """
    get:
    返回所有的模板类型
    post:
    添加模板类型
    """
    queryset = TemplateType.objects.all()
    serializer_class = TemplateTypeSerializers

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @swagger_auto_schema(request_body=TemplateTypeSerializers)
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class Template(mixins.UpdateModelMixin, generics.GenericAPIView,
               mixins.DestroyModelMixin):
    """
    delete:
    删除模板类型
    patch:
    修改模板类型
    """
    queryset = TemplateType.objects.all()
    serializer_class = TemplateTypeSerializers

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        # queryset = get_object_or_404(TemplateType, id=kwargs['id'])
        return self.update(request, *args, **kwargs)

    def perform_destroy(self, instance):
        instance.bigImage.delete()
        instance.smallImage.delete()
        instance.delete()


class PDFCovers(mixins.ListModelMixin, mixins.UpdateModelMixin,
                  mixins.CreateModelMixin, generics.GenericAPIView):
    """
    get:
    获取所有的封面
    post:
    添加封面
    """
    queryset = PC.objects.all()
    serializer_class = PDFCoverSerializers

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    parser_classes = (MultiPartParser, )
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class PDFCover(mixins.UpdateModelMixin, generics.GenericAPIView,
               mixins.DestroyModelMixin):
    """
    delete:
    删除指定封面
    patch:
    修改指定封面
    """
    queryset = PC.objects.all()
    serializer_class = PDFCoverSerializers
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def perform_destroy(self, instance):
        instance.bigImage.delete()
        instance.smallImage.delete()
        instance.delete()

    parser_classes = (MultiPartParser,)
    def patch(self, request, *args, **kwargs):
        # queryset = get_object_or_404(TemplateType, id=kwargs['id'])
        return self.update(request, *args, **kwargs)


class PDFImagesView(APIView):
    """
    get:
    查看所有的模板类型
    """
    @swagger_auto_schema(manual_parameters= [
        openapi.Parameter(
            name='layout',
            in_=openapi.IN_QUERY,
            description="模板检索纸张大小",
            type=openapi.TYPE_STRING,
            required=False,
            default='A4'
        ),
        openapi.Parameter(
            name='template',
            in_=openapi.IN_QUERY,
            description="模板检索类型,使用‘，’号隔开",
            type=openapi.TYPE_STRING,
            required=False,
            default='A,B,C'
        ),
    ])
    def get(self, request):
        layout = request.GET.get('layout', None)
        template = request.GET.get('template', None)
        pdf_image_objs = PDFImages.objects.all()
        pdf_center_seam_objs = CenterSeam.objects.all()
        if layout:
            pdf_image_objs = pdf_image_objs.filter(layout=layout)
            pdf_center_seam_objs = pdf_center_seam_objs.filter(layout=layout)
        if template and template != '':
            templates = template.split(',')
            pdf_image_objs = pdf_image_objs.filter(template_id__in=templates)
        cover_objs = PC.objects.all()
        cover_serializers = PDFCoverSerializers(cover_objs, many=True)
        serializer_class = PDFImagesSerializers(pdf_image_objs, many=True)
        center_seam_serializer = CenterSeamSerializers(pdf_center_seam_objs, many=True)
        data = {
            "coverImageList":cover_serializers.data,
            "templateImageList": serializer_class.data,
            "coverSeamImageList": center_seam_serializer.data
        }
        return Response(return_mes(code=0, data=data))


class CreatePDF(APIView):
    """
    post:
    创建族谱导出任务，并返回族谱pdf的唯一id
    """
    def post(self, request):
        create_pdf(request)
        return Response(return_mes(201))


class Health(APIView):
    """k8s健康检测接口"""
    def get(self, request):
        return Response()



class CenterSeamsView(mixins.CreateModelMixin, generics.GenericAPIView,
                    mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                     mixins.ListModelMixin):
    queryset = CenterSeam.objects.all()
    serializer_class = CenterSeamSerializers

    def get(self, request, *arg, **kwargs):
        return self.list(request, *arg, **kwargs)

    parser_classes = (MultiPartParser,)
    def post(self, request, *arg, **kwargs):
        return self.create(request, *arg, **kwargs)



class CenterSeamView(generics.GenericAPIView,
                    mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    queryset = CenterSeam.objects.all()
    serializer_class = CenterSeamSerializers

    def patch(self, request, *arg, **kwargs):
        return self.update(request, *arg, **kwargs)

    def perform_destroy(self, instance):
        instance.bigImage.delete()
        instance.smallImage.delete()
        instance.delete()

    def delete(self, request, *arg, **kwargs):
        return self.destroy(request, *arg, **kwargs)