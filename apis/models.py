from django.db import models

class TemplateType(models.Model):
    """模板种类"""
    id = models.CharField(
        primary_key=True,
        max_length=2,
        help_text="模板的标识"
    )
    modelName = models.CharField(
        max_length=10,
        help_text="模板名字",
        unique=True
    )
    def __str__(self):
        return "%s" % self.modelName


class PDFImages(models.Model):
    """pdf模板图"""
    template = models.ForeignKey(
        TemplateType,
        on_delete=models.CASCADE,
        verbose_name='template/',
        help_text="模板种类"
    )
    templateNumber = models.IntegerField(
        help_text="版式号"
    )
    bigImage = models.ImageField(
        upload_to='template/',
        help_text="图片存放位置,大文件"
    )
    smallImage = models.ImageField(
        upload_to='template/',
        help_text="图片存放位置，小文件"
    )
    layout = models.CharField(
        max_length=2,
        default="A4",
        help_text="纸张大小"
    )
    feature = models.CharField(
        max_length=255,
        default='',
        help_text='模板特点'
    )
    class Meta:
        unique_together=['template', 'templateNumber', 'layout']


class CenterSeam(models.Model):
    """中缝模板"""
    layout = models.CharField(
        max_length=2,
        default="A4",
        help_text="纸张大小"
    )
    bigImage = models.ImageField(
        upload_to='centerSeam/',
        help_text="图片存放位置,大文件"
    )
    smallImage = models.ImageField(
        upload_to='centerSeam/',
        help_text="图片存放位置，小文件"
    )
    number = models.IntegerField(
        help_text="中缝编号",
        default=0
    )


class PDFCover(models.Model):
    """模板封面图"""

    bigImage = models.ImageField(
        upload_to='cover/',
        help_text="图片存放位置,大文件"
    )
    smallImage = models.ImageField(
        upload_to='cover/',
        help_text="图片存放位置，小文件"
    )
