# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/2 下午5:18
# Description :
from rest_framework import serializers
from django.db.utils import IntegrityError
from apis.models import PDFImages, PDFCover, TemplateType, CenterSeam
from utils.return_mes import return_mes


class PDFImagesSerializers(serializers.ModelSerializer):
    """pdf模板版式，模板版式图片和模板类型对应关系"""
    LAYOUT_CHOICES = (
        ('A3', 'A3纸张大小'),
        ('A4', 'A4纸张大小')
    )
    template = serializers.CharField(max_length=2, help_text="模板代号", required=False)
    templateObj = serializers.SerializerMethodField(required=False)
    templateNumber = serializers.IntegerField(help_text="模板版式", required=False)
    bigImage = serializers.ImageField(help_text="图片存放位置,大文件", required=False)
    smallImage = serializers.ImageField(help_text="图片存放位置，小文件", required=False)
    layout = serializers.ChoiceField(LAYOUT_CHOICES, help_text="模板纸张大小", required=False)
    feature = serializers.CharField(max_length=255, required=False)

    # def to_representation(self, instance):
    #     """修改返回信息"""
    #     data = super().to_representation(instance)
    #     return data

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data.pop('template')
        return data

    def create(self, validated_data):
        template = validated_data.get('template', None)
        bigImage = validated_data.get('bigImage', None)
        smallImage = validated_data.get('smallImage', None)
        templateNumber = validated_data.get('templateNumber', None)
        layout = validated_data.get('layout')
        obj = PDFImages.objects.create(bigImage=bigImage, templateNumber=templateNumber, template_id=template,
                                       layout=layout, smallImage=smallImage)
        return obj

    def update(self, instance, validated_data):
        update_image(validated_data, instance)
        instance.template = validated_data.get('template', instance.template)
        instance.templateNumber = validated_data.get('templateNumber', instance.templateNumber)
        instance.layout = validated_data.get('layout', instance.layout)
        instance.save()
        return instance

    def get_templateObj(self, obj):
        data = {
            "templateId": obj.template.id,
            "templateName": obj.template.modelName
        }
        return data

    class Meta:
        model = PDFImages
        fields = '__all__'


class PDFCoverSerializers(serializers.ModelSerializer):
    bigImage = serializers.ImageField(help_text="图片存放位置,大文件", required=False)
    smallImage = serializers.ImageField(help_text="图片存放位置，小文件", required=False)


    def update(self, instance, validated_data):
        update_image(validated_data, instance)
        instance.save()
        return instance

    class Meta:
        model = PDFCover
        fields = '__all__'


class TemplateTypeSerializers(serializers.ModelSerializer):
    modelName = serializers.CharField(help_text="模板类型")
    id = serializers.CharField(help_text="模板的标识", max_length=2)

    def create(self, validated_data):
        try:
            obj = TemplateType.objects.create(**validated_data)
        except IntegrityError:
            raise serializers.ValidationError(return_mes(400, "该id已经存在"))
        return obj

    class Meta:
        model = TemplateType
        fields = '__all__'


class CenterSeamSerializers(serializers.ModelSerializer):
    LAYOUT_CHOICES = (
        ('A3', 'A3纸张大小'),
        ('A4', 'A4纸张大小')
    )
    id = serializers.SerializerMethodField()
    bigImage = serializers.ImageField(help_text="图片存放位置,大文件", required=False)
    smallImage = serializers.ImageField(help_text="图片存放位置，小文件", required=False)
    layout = serializers.ChoiceField(LAYOUT_CHOICES, help_text="模板纸张大小", required=False)
    # number = serializers.SerializerMethodField()

    def update(self, instance, validated_data):
        update_image(validated_data, instance)
        instance.save()
        return instance

    def get_id(self, obj):
        return obj.number

    class Meta:
        model = CenterSeam
        fields = '__all__'


def update_image(validated_data, instance):
    if validated_data.get('bigImage'):
        instance.bigImage.delete()
        instance.bigImage = validated_data.get('bigImage')
    if validated_data.get('smallImage'):
        instance.smallImage.delete()
        instance.smallImage = validated_data.get('smallImage')