# coding: utf-8 
# Author      :  王晨
# Date        :  2021/6/2 下午7:32
# Description :
import json
import os
from pathlib import Path


class Config:

    def __init__(self):
        BASE_DIR = Path(__file__).resolve().parent.parent
        if os.getenv('ENVIRON') == 'test':
            print('test环境')
            self.CONFIG_DIR = BASE_DIR.joinpath('config', 'test_config.json')
        elif os.getenv('ENVIRON') == 'master':
            print('正式环境')
            self.CONFIG_DIR = BASE_DIR.joinpath('config', 'master_config.json')
        else:
            self.CONFIG_DIR = BASE_DIR.joinpath('config', 'config.json')
        self.APIS_DIR = BASE_DIR.joinpath('config', "urls.json")
        self.DEBUG = False
        self.APIS = {}
        self.BACKEND = {}
        self.ALI_OSS = {}
        self.RABBITMQ = {}
        self.DATABASE = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'POOL': {  # 更多的配置请参考DBUtils的配置
                    'minsize': 5,  # 初始化时，连接池中至少创建的空闲的链接，0表示不创建，不填默认为5
                    'maxsize': 0,  # 连接池中最多闲置的链接，0不限制，不填默认为0
                    'maxconnections': 0,  # 连接池允许的最大连接数，0表示不限制连接数, 默认为0
                    'blocking': True,  # 连接池中如果没有可用连接后，是否阻塞等待。True:等待；False:不等待然后报错, 默认False
                },
            }
        }
        self.config_load()

    def config_load(self):
        with self.CONFIG_DIR.open('r') as f:
            config = json.loads(f.read())
        with self.APIS_DIR.open('r') as fd:
            self.APIS = json.loads(fd.read())
        self.database(config['DATABASE'])
        self.debug(config)
        self.backend(config)

    def database(self, config):
        """数据库配置"""
        self.DATABASE['default'].update(config['MYSQL'])

    def debug(self, config):
        """debug配置"""
        self.DEBUG = config['DEBUG']

    def backend(self, config):
        self.BACKEND = config['BACKEND']


